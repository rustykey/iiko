$(function() {
  $('.js__submenu').each(function() {
    var $submenu = $(this),
      $in = $submenu.find('.js__submenu-in'),
      offset,
      to,

      CLASS_STICKED = 'is__sticked';

    var getOffset = function() {
      return $submenu.offset().top
    }
    var checkScroll = function() {
      if ($(document).scrollTop() > offset) {
        $in.addClass(CLASS_STICKED);
      }
      else {
        $in.removeClass(CLASS_STICKED);
      }
    }

    offset = getOffset();

    $(window).on('resize', function() {
      clearTimeout(to);
      setTimeout(function() {
        offset = getOffset();
        checkScroll();
      }, 100);
    })

    $(document).on('scroll', function() {
      checkScroll();
    });

  });
});
