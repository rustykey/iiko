function HomeControl(controlDiv, map, zoomin, zoomout) {
 google.maps.event.addDomListener(zoomout, 'click', function() {
   var currentZoomLevel = map.getZoom();
   if(currentZoomLevel != 0){
     map.setZoom(currentZoomLevel - 1);}
  });

 google.maps.event.addDomListener(zoomin, 'click', function() {
   var currentZoomLevel = map.getZoom();
   if(currentZoomLevel != 21){
     map.setZoom(currentZoomLevel + 1);}
  });

}

function initialize(canvas, coords, zoomin, zoomout) {
  var latLng = new google.maps.LatLng(coords[0],coords[1]);
  var mapOptions = {
    center: latLng,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false,
    disableDefaultUI: true
  };
  var map = new google.maps.Map(canvas, mapOptions);
  var marker = new google.maps.Marker({
      position: latLng,
      map: map
  });

  var homeControlDiv = document.createElement('div');
  var homeControl = new HomeControl(homeControlDiv, map, zoomin, zoomout);
}

// google.maps.event.addDomListener(window, 'load', initialize);

$(function() {
  var id = 0;
  $('.js__cmap').each(function() {
    initialize($(this).find('.js__cmap-in')[0], $(this).data('coords').split(','), $(this).find('.js__cmap-zoom-in')[0], $(this).find('.js__cmap-zoom-out')[0])
  });
})
