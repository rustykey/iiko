$ ->
  $(".js__toggler").each ->
    $toggler = $(this)
    $box = $($toggler.attr("href"))

    noclose = $toggler.data("noclose")

    CLASS_ACTIVE = "is__active"


    $toggler.on "click", (e) ->
      $toggler.toggleClass CLASS_ACTIVE
      $box.toggleClass CLASS_ACTIVE
      false

    unless noclose
      $(document).on "click", ->
        $box.removeClass CLASS_ACTIVE
        return

    $box.on "click", (e) ->
      e.stopPropagation()
      return

    return


  $(".js__tt").each ->
    $box = $(this)
    $toggler = $box.find('.js__tt-toggler')

    CLASS_ACTIVE = "is__active"

    $toggler.on "click", (e) ->
      $box.toggleClass CLASS_ACTIVE
      false

    return

  return
