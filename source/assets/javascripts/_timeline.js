$(function() {
  $('.js__timeline').each(function() {
    var $box = $(this),
      $loader = $box.find('.js__timeline-loader'),
      $placeholder = $box.find('.js__timeline-placeholder'),

      CLASS_LOADING = 'is__loading',
      CLASS_READY = 'is__ready';

    $loader.on('click', function(e) {
      e.preventDefault();
      var h = $box.height();
      $box.height(h).addClass(CLASS_LOADING);
      $.get('/assets/data/events.json', function(data) {
        var $elems = $(data.body);
        $placeholder.append($elems);
        h += $elems.height();
        $box.height(h).removeClass(CLASS_LOADING);
        $elems.addClass(CLASS_READY);
        $box.css('height', 'auto');
      })
    });
  });
});
