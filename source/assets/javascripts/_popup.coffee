$ ->
  $wrapper = $("#js__wrapper")
  CLASS_BLURRED = "is__blurred"
  DURATION = 400
  $(".js__plink").magnificPopup
    type: "inline"
    removalDelay: DURATION
    closeMarkup: "<button title=\"%title%\" class=\"mfp-close\"></button>"
    callbacks:
      beforeOpen: ->
        $wrapper.addClass CLASS_BLURRED
        return

      beforeClose: ->
        $wrapper.removeClass CLASS_BLURRED
        return

  return
