$ ->
  total = 0;
  $(".js__order").each ->
    $box = $(this)
    $toggler = $box.find(".js__order-toggler")
    $sum = $box.find(".js__order-sum")
    $plus = $box.find(".js__order-plus")
    $minus = $box.find(".js__order-minus")
    $counter = $('.js__cart-counter')
    $counterOld = $counter.find('.js__cart-counter-old')
    $counterNew = $counter.find('.js__cart-counter-new')

    sum = 0
    toSum = undefined
    toTotal = undefined

    CLASS_ACTIVE = "is__active"
    CLASS_UPDATED = "is__updated"
    DURATION = 400


    updateSum = (sum, total) ->
      $sum.text sum
      $box.addClass CLASS_UPDATED

      $counterNew.text(total)
      $counter.addClass CLASS_UPDATED

      clearTimeout = toSum
      toSum = setTimeout(->
        $box.removeClass CLASS_UPDATED
        $counter.removeClass CLASS_UPDATED
        return
      , DURATION * 2)

      clearTimeout = toTotal
      toTotal = setTimeout(->
        $counterOld.text(total)
      , DURATION)

      return

    $plus.on "click", (e) ->
      e.preventDefault()
      updateSum ++sum, ++total
      return

    $minus.on "click", (e) ->
      e.preventDefault()
      if sum > 0
        updateSum --sum, --total
      return

    $toggler.on "click", (e) ->
      e.preventDefault()
      $box.addClass CLASS_ACTIVE
      return

    return

  return
