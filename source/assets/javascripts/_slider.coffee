$ ->
  $(".js__slider").each ->
    $slider = $(this)
    $navPrev = $slider.find(".js__slider-nav-prev")
    $navNext = $slider.find(".js__slider-nav-next")
    $list = $slider.find(".js__slider-list")

    offset = 0
    maxOffset = $list.children().length - 1

    CLASS_DISABLED = "is__disabled"


    slideTo = (ofst) ->
      $list.css left: -ofst * 100 + "%"
      return

    $navPrev.on "click", (e) ->
      e.preventDefault()
      if offset > 0
        $navNext.removeClass CLASS_DISABLED
        offset--
        slideTo offset
        $navPrev.addClass CLASS_DISABLED  if offset is 0
      return

    $navNext.on "click", (e) ->
      e.preventDefault()
      if offset < maxOffset
        $navPrev.removeClass CLASS_DISABLED
        offset++
        slideTo offset
        $navNext.addClass CLASS_DISABLED  if offset is maxOffset
      return

    return

  return
