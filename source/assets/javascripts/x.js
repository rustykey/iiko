//= require lib/magnific
//= require lib/fitvid
//= require lib/imagesloaded

//= require _popup
//= require _carousel
//= require _toggler
//= require _slider
//= require _tbox
//= require _order
//= require _timeline
//= require _eqs
//= require _submenu

$(".scr-video-box").fitVids();
