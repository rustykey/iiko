$ ->
  $(".js__tbox").each ->
    $box = $(this)
    $inner = $box.find(".js__tbox-in")
    $toggler = $box.find(".js__tbox-toggler")
    $hid = $box.find(".js__tbox-hid")
    totalHeight = $inner.height() + $hid.height()
    to = undefined
    CLASS_ACTIVE = "is__active"
    $box.height $box.height()
    $toggler.on "click", (e) ->
      e.preventDefault()
      $box.addClass CLASS_ACTIVE
      $box.height totalHeight
      setTimeout (->
        $toggler.parent().remove()
        return
      ), DURATION
      return

    return

  return


# $(window).resize(function() {
#   clearTimeout(to);
#   to = setTimeout(function() {
#     $box.height($box.height());
#   }, 100)
# })
