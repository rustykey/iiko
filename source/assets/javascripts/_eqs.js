$(function() {
  $('.js__eqs').each(function() {
    var $items = $(this).find('.js__eqs-item'),
      h = 0;

    imagesLoaded($(this), function() {
      $items.each(function() {
        var hItem = $(this).height();
        if (h < hItem) h = hItem;
      });

      $items.height(h);
    })
  })
});
