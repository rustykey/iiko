$ ->
  $(".js__crsl").each ->
    $slider = $(this)
    $dotsBox = $slider.find(".js__crsl-dots")
    $dotsBoxIn = $dotsBox.find(".js__crsl-dots-in")
    $dots = $dotsBox.find(".js__crsl-dot")
    $itemsBox = $slider.find(".js__crsl-items")
    $itemsList = $itemsBox.find(".js__crsl-list")
    $items = $itemsBox.find(".js__crsl-item")
    $navBox = $slider.find(".js__crsl-nav")
    $navPrev = $navBox.find(".js__crsl-nav-prev")
    $navNext = $navBox.find(".js__crsl-nav-next")
    $dotsBoxIn = $dotsBox.find(".js__crsl-dots-in")

    current = 0
    size = $items.length
    offset = undefined
    windowWidth = $(window).width()
    dotsWidth = $dotsBoxIn.width()
    dotWidth = $dots.first().outerWidth(true)
    resizeTo = undefined
    isCarousel = !!$slider.data('carousel')

    IS_VERTICAL = !!$slider.data("slider-vert")
    CLASS_ACTIVE = "is__active"

    if IS_VERTICAL
      offset = $slider.height()
    else
      offset = $slider.width()

    redot = (index) ->
      $dots.removeClass CLASS_ACTIVE
      $dots.filter(':nth-child(' + (index + 1) + ')').addClass CLASS_ACTIVE

      $items.removeClass CLASS_ACTIVE
      $items.filter(':nth-child(' + (index + 1) + ')').addClass CLASS_ACTIVE

      if dotsWidth > windowWidth
        if isCarousel
          offset = (index * dotWidth) - (windowWidth - 840)/2
        else
          offset = index / (size - 1) * (dotsWidth - windowWidth)
        $dotsBox.css "margin-left": -offset + "px"
      return

    slideTo = (index) ->
      redot index

      isVertical = !!$slider.data("slider-vert")
      if isVertical
        $itemsList.css
          "margin-top": -425 * index
          "margin-left": -(index * 100) + "%"

      else
        $itemsList.css "margin-left": -(index * 100) + "%"
      index

    initCarousel = () ->
      $dotsBox.append($dotsBoxIn.clone())
      $dotsBox.append($dotsBoxIn.clone())
      $dotsBox.css("left", - dotsWidth)

    initDots = () ->
      $dots = $dotsBox.find(".js__crsl-dot")
      $dots.on "click", (e) ->
        e.preventDefault()
        index = $(this).index()
        current = slideTo(index)  unless index is current
        return
      return

    $navPrev.on "click", (e) ->
      e.preventDefault()
      current = slideTo(--current)  if current > 0
      return

    $navNext.on "click", (e) ->
      e.preventDefault()
      current = slideTo(++current)  if current < size - 1
      return

    $(window).on "resize", ->
      clearTimeout resizeTo
      resizeTo = setTimeout(->
        windowWidth = $(window).width()
        redot current
        return
      , 100)
      return

    if isCarousel
      initCarousel()
      initDots()
      redot current
    else
      initDots()

    return

  return
