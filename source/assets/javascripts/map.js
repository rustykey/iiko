//= require 'lib/markerclusterer'

function HomeControl(controlDiv, map) {

 google.maps.event.addDomListener(zoomout, 'click', function() {
   var currentZoomLevel = map.getZoom();
   if(currentZoomLevel != 0){
     map.setZoom(currentZoomLevel - 1);}
  });

   google.maps.event.addDomListener(zoomin, 'click', function() {
   var currentZoomLevel = map.getZoom();
   if(currentZoomLevel != 21){
     map.setZoom(currentZoomLevel + 1);}
  });

}

function initialize() {
  var style = [{
    url: '/assets/images/cluster.png',
    width: 46,
    height: 64,
    textColor: '#000000',
    textSize: 14
  },{
    url: '/assets/images/cluster.png',
    width: 46,
    height: 64,
    textColor: '#000000',
    textSize: 14
  },{
    url: '/assets/images/cluster.png',
    width: 46,
    height: 64,
    textColor: '#000000',
    textSize: 14
  }];
  var center = new google.maps.LatLng(53.165448, 26.359065);
  var options = {
    zoom: 5,
    center: center,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false,
    disableDefaultUI: true
  };

  var map = new google.maps.Map(document.getElementById("map"), options);

  var markers = [];

  $.getJSON('/assets/data/map.json', function(data) {
    for (var i = 0; i < 100; i++) {
      var latLng = new google.maps.LatLng(data.photos[i].latitude,
          data.photos[i].longitude);
      var marker = new google.maps.Marker({
        'position': latLng,
        'icon': new google.maps.MarkerImage('/assets/images/dot.png', new google.maps.Size(11, 11))
      });
      markers.push(marker);
    }
    var markerCluster = new MarkerClusterer(map, markers, {
      gridSize: 100,
      styles: style
    });
  })

  var homeControlDiv = document.createElement('div');
  var homeControl = new HomeControl(homeControlDiv, map);
}

google.maps.event.addDomListener(window, 'load', initialize);

